#include <stdio.h>
#include <pthread.h>

 class BoundedBuffer
    {
    public:
        BoundedBuffer() { head = 0; tail = 0; }
    
        void put(char ch) {
            QMutexLocker locker(&mutex);
            while (tail == head + N)
                bufferIsNotFull.wait(&mutex);
            buffer[tail++ % N] = ch;
            bufferIsNotEmpty.wakeOne();
        }
        char get() {
            QMutexLocker locker(&mutex);
            while (head == tail)
                bufferIsNotEmpty.wait(&mutex);
            char ch = buffer[head++ % N];
            bufferIsNotFull.wakeOne();
            return ch;
        }
    
    private:
        QMutex mutex;
        QWaitCondition bufferIsNotFull;
        QWaitCondition bufferIsNotEmpty;
        int head, tail;
        char buffer[N];
    };